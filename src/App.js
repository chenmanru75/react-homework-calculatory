import React from 'react';
import './App.css';
import Calculatory from "./Calculatory";

class App extends React.Component {
    render(){
        return (
            <div className="App">
                <Calculatory/>
            </div>
        );
    }
}

export default App;
