import React, {Component} from 'react';

class Calculatory extends Component {


    constructor(props, context) {
        super(props, context);
        this.state = {
            number: '',
            antherNumber: '',
            status:'',
            result:''
        };
        this.handleChangeInput = this.handleChangeInput.bind(this);
        this.handleChangeInputAnther = this.handleChangeInputAnther.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleSub = this.handleSub.bind(this);
        this.handleMul = this.handleMul.bind(this);
        this.handleDid = this.handleDid.bind(this);
        this.handleCalculate = this.handleCalculate.bind(this);
        this.handleAC = this.handleAC.bind(this);
    }

    handleChangeInput(e){
        this.setState({
            number: e.target.value
        })
    }

    handleChangeInputAnther(e){
        this.setState({
            antherNumber: e.target.value
        })
    }

    handleAdd(){
        this.setState({
            status:'+'
        })
    }

    handleSub(){
        this.setState({
            status:'-'
        })
    }

    handleMul(){
        this.setState({
            status:'*'
        })
    }

    handleDid(){
        this.setState({
            status:'/'
        })
    }

    handleCalculate(status){
        let {number ,antherNumber} = this.state;
        let result = 0;
        switch (status) {
            case '+': result = number*1 + antherNumber*1;break;
            case '-': result = number - antherNumber;break;
            case '*': result = number * antherNumber;break;
            case '/': result = number / antherNumber;break;
        }
        this.setState({
            result:result
        })
    }

    handleAC(){
        this.setState({
            number:'',
            antherNumber:'',
            result:''
        })
    }

    render() {
        const style = {
            wrap:{
                width: '400px',
                height: '280px',
                backgroundColor: 'beige',
                borderRadius:'20%',
                marginLeft: '400px',
                paddingTop:'20px',
                border:'1px solid #aaa',
                marginTop: '50px',
            },
            output:{
                width: '240px',
                height:'50px',
                backgroundColor: '#eee',
                marginLeft: '70px',
                borderRadius: "20px",
                border:'1px solid #aaa',
                fontSize:'35px',
                textAlign:'right',
                paddingRight: "20px"
            },
            input:{
                display: 'flex'
            },
            calculate:{
                width:'140px',
                listStyle:'none',
                paddingLeft:'70px'
            },
            inputNum:{
                flex:1
            },
            status:{
                width:'50px',
                height:'50px',
                backgroundColor:'darkorange',
                color:"#fff",
                borderRadius: "50%",
                fontSize: '36px',
                border:'1px solid #aaa',
                float:'left',
                marginRight: '10px',
                marginTop:'10px'
            },
            number:{
                width:'60px',
                height:'30px',
                borderRadius: "20px",
                float: 'left',
                marginLeft:'20px',
                marginTop:'30px',
                outline:'none',
                fontSize: '20px',
                textAlign:'right',
                paddingRight: '20px'
            },
            btn:{
                width:'100px',
                height:'50px',
                backgroundColor:'#61dafb',
                borderRadius: "30px",
                fontSize: '20px',
                color: '#fff',
                marginLeft:'20px',
                outline:'none',

            }
        };

        const {number,antherNumber,status,result} = this.state;
        return (
            <div style={style.wrap}>
                <div style={style.output}>{result}</div>
                <div style={style.input}>
                    <ul style={style.calculate}>
                        <li style={style.status} onClick={this.handleAdd}>+</li>
                        <li style={style.status} onClick={this.handleSub}>-</li>
                        <li style={style.status} onClick={this.handleMul}>*</li>
                        <li style={style.status} onClick={this.handleDid}>/</li>
                    </ul>
                    <div style={style.inputNum}>
                        <input value={number} onChange={this.handleChangeInput} style={style.number}/><br/>
                        <input value={antherNumber}  onChange={this.handleChangeInputAnther}  style={style.number}/>
                    </div>
                </div>
                <div>
                    <button style={style.btn} onClick={this.handleAC}>AC</button>
                    <button style={style.btn} onClick={() => this.handleCalculate(status)}>=</button>
                </div>
            </div>
        );
    }
}

export default Calculatory;